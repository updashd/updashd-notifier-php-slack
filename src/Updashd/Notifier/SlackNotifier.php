<?php

namespace Updashd\Notifier;

use Updashd\Configlib\Config;

class SlackNotifier implements NotifierInterface {
    const FIELD_WEBHOOK_URL = 'webhook_url';
    const FIELD_CHANNEL = 'channel';
    const FIELD_DETAIL_LEVEL = 'detail_level';
    const FIELD_ICON_EMOJI = 'icon_emoji';
    const FIELD_ICON_URL = 'icon_url';
    const FIELD_NAME = 'name';
    const FIELD_BASE_URL = 'base_url';

    const LEVEL_LINK = 0;
    const LEVEL_MSG = 1;
    const LEVEL_NAMES = 2;
    const LEVEL_FIELDS = 3;
    const LEVEL_ALL = 4;

    /** @var \Updashd\Configlib\Config $config */
    protected $config;

    public static function getReadableName () {
        return 'Slack';
    }

    public static function getNotifierName () {
        return 'slack';
    }

    public static function createConfig () {
        $config = new Config();

        $config->addFieldText(self::FIELD_WEBHOOK_URL, 'Webhook URL', null, true);
        $config->addFieldText(self::FIELD_CHANNEL, 'Destination (#chan/@user)', '#general', true);
        $config->addFieldSelect(self::FIELD_DETAIL_LEVEL, 'Notification Detail Level', self::getDetailLevelOptions(), self::LEVEL_MSG);

        $config->addFieldText(self::FIELD_NAME, 'Bot Name', 'Updashd', false);

        $config->addFieldText(self::FIELD_ICON_EMOJI, 'Icon (:emoji:)', null, false);
        $config->addFieldText(self::FIELD_ICON_URL, 'Icon (URL)', null, false);

        $config->addFieldUrl(self::FIELD_BASE_URL, 'Base URL', 'https://app.updashd.com', true);

        return $config;
    }

    public function __construct (Config $config) {
        $this->config = $config;
    }

    private static function getDetailLevelOptions () {
        return [
            self::LEVEL_LINK => 'Incident Link Only',
            self::LEVEL_MSG => 'Message & Incident Link',
            self::LEVEL_NAMES => 'Message & Name Fields',
            self::LEVEL_FIELDS => 'Message & All Fields',
            self::LEVEL_ALL => 'Everything',
        ];
    }

    public function send (NoticeData $noticeData) {
        $incident = $noticeData->getIncident();

        $errorCode = $incident->getMessageCode();
        $errorMessage = $incident->getMessage();

        $accountSlug = $incident->getAccount()->getSlug();
        $incidentId = $incident->getIncidentId();

        $hookUrl = $this->config->getValueRequired(self::FIELD_WEBHOOK_URL);
        $updashdHostname = $this->config->getValueRequired(self::FIELD_BASE_URL, true);
        $detailLevel = $this->config->getValueRequired(self::FIELD_DETAIL_LEVEL, true);

        $viewIncidentUrl = $updashdHostname . '/account/' . $accountSlug . '/incident/detail?iid=' . $incidentId;

        $text = '<' .$viewIncidentUrl . '|View Incident #' . $incidentId . '>';

        if ($detailLevel >= self::LEVEL_MSG) {
            $text = $errorMessage . ' ' . $text;
        }

        $fields = [];

        if ($detailLevel >= self::LEVEL_NAMES) {
            $fields[] = ['title' => 'Node', 'value' => $incident->getNodeService()->getNode()->getNodeName(), 'short' => true];
            $fields[] = ['title' => 'Node Service', 'value' => $incident->getNodeService()->getNodeServiceName(), 'short' => true];
            $fields[] = ['title' => 'Service', 'value' => $incident->getNodeService()->getService()->getServiceName(), 'short' => true];
            $fields[] = ['title' => 'Environment', 'value' => $incident->getNodeService()->getNode()->getEnvironment()->getEnvironmentName(), 'short' => true];
        }

        if ($detailLevel >= self::LEVEL_FIELDS) {
            $fields[] = ['title' => 'Error Code', 'value' => $errorCode, 'short' => true];
            $fields[] = ['title' => 'Error Count', 'value' => count($incident->getResults()), 'short' => true];

            $fields[] = ['title' => 'Severity', 'value' => $incident->getSeverity()->getSeverityName(), 'short' => true];
            $fields[] = ['title' => 'First Seen', 'value' => $this->getSlackDate($incident->getDateFirstSeen()), 'short' => true];
            $fields[] = ['title' => 'Last Seen', 'value' => $this->getSlackDate($incident->getDateLastSeen()), 'short' => true];
        }

//        if ($detailLevel >= self::LEVEL_ALL) {
//            // Get the latest result
//            foreach ($incident->getResults() as $result) {
//                foreach ($result->getResultMetrics() as $resultMetric) {
//                    $metricType = $resultMetric->getMetricType();
//                    $typeId = $metricType->getMetricTypeId();
//
//                    if (in_array($typeId, [
//                        $metricType::TYPE_FLOAT,
//                        $metricType::TYPE_INT,
//                        $metricType::TYPE_STR
//                    ])) {
//                        $fields[] = [
//                            'title' => $resultMetric->getFieldName(),
//                            'value' => $resultMetric->getValueAuto(),
//                            'short' => true
//                        ];
//                    }
//                }
//
//                // Don't care about the rest
//                break;
//            }
//        }

        $color = $this->translateColor($incident->getSeverity()->getBootstrapColor());

        $this->slack($hookUrl, $text, $color, $fields);
    }

    protected function translateColor ($color) {
        $map = [
            'success' => 'good',
            'info' => '#5bc0de', // Of course
            'warning' => 'warning',
            'danger' => 'danger',
        ];

        return array_key_exists($color, $map) ? $map[$color] : '#000000';
    }

    protected function conditionallySetOption (&$options, $optionFieldKey, $configFieldKey, $canUseDefault = false) {
        $value = $this->config->getValue($configFieldKey, $canUseDefault);

        if ($value) {
            $options[$optionFieldKey] = $value;
        }
    }

    /**
     * @param \DateTime $time
     * @param string $format
     * @param string $url
     * @param string $fallback
     * @return string
     */
    protected function getSlackDate ($time, $format = '{date_num} {time_secs}', $url = null, $fallback = null) {
        $timestamp = $time->getTimestamp();

        $fallback = $fallback ?: $time->format(DATE_ISO8601);

        return '<!date^' . $timestamp . '^' . $format . ($url ? '^' . $url : '') . '|' . $fallback . '>';
    }

    /**
     * @param string $url Incoming Webhook URL
     * @param string $message
     * @param string $color "good", "warning", "danger", "#FF00FF"
     * @param array $fields
     * @return mixed
     */
    protected function slack ($url, $message, $color, $fields) {
        $options = [
            'fallback' => $message,
            'text' => $message,
            'color' => $color, //
            'fields' => $fields
        ];

        $this->conditionallySetOption($options, 'username', self::FIELD_NAME);
        $this->conditionallySetOption($options, 'icon_emoji', self::FIELD_ICON_EMOJI);
        $this->conditionallySetOption($options, 'icon_url', self::FIELD_ICON_URL);

        $data = "payload=" . json_encode($options);

        // You can get your webhook endpoint from your Slack settings
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}